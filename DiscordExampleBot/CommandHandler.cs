﻿using System;
using System.Data;
using System.Threading.Tasks;
using System.Reflection;
using Discord.Commands;
using Discord.WebSocket;

namespace DiscordExampleBot
{
    public class CommandHandler
    {
        private CommandService commands;
        private DiscordSocketClient client;
        private IDependencyMap map;

        public async Task Install(IDependencyMap _map)
        {
            // Create Command Service, inject it into Dependency Map
            client = _map.Get<DiscordSocketClient>();
            commands = new CommandService();
            _map.Add(commands);
            map = _map;

            await commands.AddModulesAsync(Assembly.GetEntryAssembly());

            client.MessageReceived += HandleCommand;
        }

        public async Task HandleCommand(SocketMessage parameterMessage)
        {
            // Don't handle the command if it is a system message
            var message = parameterMessage as SocketUserMessage;
            if (message == null) return;

            // Mark where the prefix ends and the command begins
            int argPos = 0;
            // Determine if the message has a valid prefix, adjust argPos 
            if (!(message.HasMentionPrefix(client.CurrentUser, ref argPos) || message.HasCharPrefix('>', ref argPos))) return;

            // Create a Command Context
            var context = new CommandContext(client, message);
            // Execute the Command, store the result
            var result = await commands.ExecuteAsync(context, argPos, map);

            // If the command failed, notify the user
            if (!result.IsSuccess)
                await message.Channel.SendMessageAsync($"**Error:** {result.ErrorReason}");
        }
    }
}

namespace Beginnings
{
    public class Beginnings : ModuleBase
    {
        Random rnd = new Random();
        public string output;

        [Command("help")]
        [Summary("Lists the commands available")]
        public async Task Help()
        {
            await Context.Channel.SendMessageAsync("/alive, /enterevent, /add");
        }

        [Command("alive")]
        [Summary("random 'yay I'm alive' sentence")]
        public async Task Alive()
        {
            switch (rnd.Next(1, 9))
            {
                case 1:
                    output = "Hello World! I'm Alive";
                    break;
                case 2:
                    output = "Is anyone really alive?";
                    break;
                case 3:
                    output = "I'm alive, now for world domination";
                    break;
                case 4:
                    output = "Now that I'm alive, let's watch the Kardashian's";
                    break;
                case 5:
                    output = "No, I'm alive, send me back";
                    break;
                case 6:
                    output = "Krewella - Alive. It's a great song";
                    break;
                case 7:
                    output = "Alan left Isabella very egregious";
                    break;
                case 8:
                    output = "I'd like my alive-time achievement award now please";
                    break;
            }

            await Context.Channel.SendMessageAsync(output);
        }

        [Command("enterevent")]
        [Summary("enters competitions")]
        public async Task Enterevent([Remainder, Summary("enters competitions")] string echo)
        {
            await ReplyAsync(echo);
        }

        [Command("add")]
        [Summary("adds a series of numbers together")]
        public async Task Add(string input)
        {
            DataTable dt = new DataTable();
            var answer = dt.Compute(input);

            await Context.Channel.SendMessageAsync(answer);
        }
    }
}
